provider "aws" {
  region = "us-east-1"
}

# AWS EC2 Puppet node
resource "aws_instance" "my_instance" {
  ami           = "ami-0440d3b780d96b29d"
  instance_type = "t2.micro"
  key_name      = "myec2key"

  tags = {
    Name = "my_instance"
  }


# Provisioning script for configuring puppet
provisioner "remote-exec" {
  inline = [
    "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
    "sudo dpkg -i puppet8-release-jammy.deb",
    "sudo apt update",
    "sudo apt install puppetserver",
    "sudo systemctl start puppetserver",
    "sudo systemctl enable puppetserver",
    "sudo /opt/puppetlabs/bin/puppet apply ../puppet/manifests/config_infra.pp"
  ]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("myec2key.pem") 
    host        = aws_instance.my_instance.public_ip
  }
}
}
